# test/models/item_test.rb
require 'test_helper'

class ItemTest < ActiveSupport::TestCase
  def setup
    @parameters = { title: "title", description: "a super description" }
  end

  test "it should not be created without title" do
    @parameters.reject! {|k, v| k == :title}
    assert_raises (ActiveRecord::RecordInvalid){ Item.create!(@parameters) }
  end

  test "it should not be created without description" do
    @parameters.reject! {|k, v| k == :description}
    assert_raises (ActiveRecord::RecordInvalid) { Item.create!(@parameters) }
  end

  test "it should not be done at creation" do
    @parameters.merge!({ is_done: true })
    item = Item.create!(@parameters)
    assert_not item.is_done
  end

  test "it should be created with full informations" do
    item = Item.create!(@parameters)
    assert item.valid?
    assert_not item.is_done
  end
end