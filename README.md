rails _5.1.0_ new . --api --webpack=react 
npm install html-webpack-plugin --save-dev

```javascript
const environment = require('./environment')
const HtmlWebpackPlugin = require('html-webpack-plugin')

environment.plugins.set('HtmlWebpackPlugin', new HtmlWebpackPlugin({
  title: 'My ReactJs App',
  inject: true
}))
module.exports = environment.toWebpackConfig()
```

Ajouter au Gemfile `gem 'react-rails', '~> 2.3'`, lancer le `bundle`.

```ruby
$ rails generate react:install
Running via Spring preloader in process 12819
      create  app/javascript/components
      create  app/javascript/components/.gitkeep
warning package.json: No license field
warning No license field
warning fsevents@1.1.3: The platform "linux" is incompatible with this module.
warning No license field
      append  app/javascript/packs/application.js
      create  app/javascript/packs/server_rendering.js
```

https://daveceddia.com/ajax-requests-in-react/


Single Page Application (SPA)

$ yarn add react-router react-router-dom