class ItemsController < ApplicationController
  def index
    render json: Item.all
  end

  def create
    item = Item.new(item_params)
    if item.save
      render json: item
    else 
      render json: {error: item.errors.full_messages}, status: 422
    end
  end
  
  private
    def item_params
      params.permit(:title, :description, :is_done)
    end
end
