# app/models/items.rb
class Item < ApplicationRecord
  validates_presence_of :title
  validates_presence_of :description
  before_create :init_attributes

  private

    def init_attributes
      self.is_done = false
    end
end