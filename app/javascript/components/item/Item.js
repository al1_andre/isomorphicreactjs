import React from "react"
import PropTypes from "prop-types"

class Item extends React.Component {
  render () {
    return (
      <div>
        <div>Title: {this.props.title}</div>
        <div>Description: {this.props.description}</div>
        <div>Is Done: {this.props.isDone}</div>
      </div>
    );
  }
}

Item.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  isDone: PropTypes.bool
};
export default Item
