import React from "react"
import PropTypes from "prop-types"
import axios from 'axios';

class NewItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      description: ""
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({[name]: value});
  }
  
  handleSubmit(event) {
    event.preventDefault();
    axios.post("/items", {
      title: this.state.title,
      description: this.state.description
    }).then( response => {
      this.props.parent.refresh();
      this.props.parent.closeModal();
    }).catch(function (error) {
      console.log(error.response.data.error);
    });
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit} >
        <label>
          Title :
          <input
            name="title"
            type="text"
            value={this.state.title}
            onChange={this.handleInputChange} />
        </label>
        <br />
        <label>
          Description :
          <input
            name="description"
            type="text"
            value={this.state.description}
            onChange={this.handleInputChange} />
        </label>
        <input type="submit" value="Submit" />
      </form>
    );
  }
}

NewItem.propTypes = {
  parent: PropTypes.object
};

export default NewItem