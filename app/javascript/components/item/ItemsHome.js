import React from "react"
import axios from 'axios';
import { Route, Link } from 'react-router-dom';
import ItemList from "./ItemList"
import NewItem from "./NewItem"
import Modal from 'react-modal';

class ItemsHome extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      items: [],
      modalIsOpen: false
    };

    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.refresh = this.refresh.bind(this);
  }
  openModal() {
    this.setState({modalIsOpen: true});
  }
  closeModal() {
    this.setState({modalIsOpen: false});
  }
  render () {
    return (
      <div>
        <div>
          <div>
            <button onClick={this.openModal}>Nouveau</button>
            <Modal
              isOpen={this.state.modalIsOpen}
              onRequestClose={this.closeModal}
              contentLabel="Example Modal" >
              <h2>Création d'un nouvel Item</h2>
              <button onClick={this.closeModal}>Fermer</button>
              <NewItem parent={this}/>
            </Modal>
          </div>
        </div>
        <ItemList items={this.state.items} />
      </div>
    );
  }
  componentDidReceiveProps(newProps){
    this.refresh();
  }
  componentDidMount() {
    this.refresh();
  }
  refresh(){
    axios.get("/items")
      .then( res => {
        this.setState({items: res.data});
      });
  }
}


export default ItemsHome
