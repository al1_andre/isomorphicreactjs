import React from "react"
import PropTypes from "prop-types"
import Item from "./Item"

class ItemList extends React.Component {
  render () {
    return (
      <div>
        {
          this.props.items.map(function(item){
            const id = "item-"+item.id
            return (
              <div key={id}>
                <Item title={item.title} description={item.description} isDone={item.isDone} />
              </div>
            )
          })
        }
      </div>
    );
  }
}

ItemList.propTypes = {
  items: PropTypes.array
};

export default ItemList
