import React from "react"
import { BrowserRouter as Router, Switch, Route, NavLink } from 'react-router-dom';
import Home from './Home'
import ItemsHome from './item/ItemsHome'

class App extends React.Component {
  
  render () {
    return (
      <Router>
        <div>
          <h2>Une petite Single Page Application (SPA) ReactJs</h2>
          <ul>
            <li><NavLink to={'/'} activeClassName="active">Home</NavLink></li>
            <li><NavLink to={'/items'} activeClassName="active">Items</NavLink></li>
          </ul>
          <Switch>
            <Route exact path='/' component={Home} />
            <Route path='/items' component={ItemsHome} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App
