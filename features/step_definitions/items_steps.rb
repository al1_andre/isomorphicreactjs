#features/step_definitions/items_steps.rb
Given(/^the following items exist:$/) do |table|
  # table is a Cucumber::MultilineArgument::DataTable
  table.hashes.each do |obj|
    Item.create!(obj)
  end
end

Given(/^I am on the items index$/) do
  visit "items"
end

Then(/^I should see "([^"]*)"$/) do |arg1|
  byebug
  assert page.has_content?(arg1)
end

When(/^I click on "([^"]*)"$/) do |arg1|
  click_on(arg1)
end

When(/^I fill in "([^"]*)" with "([^"]*)"$/) do |arg1, arg2|
  fill_in(arg1, :with => arg2)
end

Then(/^I should be on ([^"]*) new page$/) do |arg1|
  visit send("new_"+"#{arg1}".singularize+"_path")
end

Then(/^I should be on ([^"]*) index page$/) do |arg1|
  visit send("#{arg1}_path")
end