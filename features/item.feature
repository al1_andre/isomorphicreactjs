# todo_list/features/item.feature
Feature: Managing items
  As a user
  So that I can go to shopping 
  I want to see my shopping list

Background:
  Given the following items exist:
  |id|title |description|is_done|
  |1 |Item 1|bla        |false  |
  |2 |Item 2|blabla     |false  |

Scenario: the view should show the shopping list
  Given I am on the items index
  Then I should see "Item 1"
  
Scenario: The view should allow to create a new item
  Given I am on the items index
  When I click on "New"
  Then I should be on items new page
  When I fill in "title" with "Go to Apple store"
  And I fill in "description" with "Buy Iphone X"
  And I click on "Submit"
  Then I should be on items index page
  And I should see "Go to Apple store"