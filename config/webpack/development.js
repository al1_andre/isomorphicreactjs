const environment = require('./environment')
const HtmlWebpackPlugin = require('html-webpack-plugin')

environment.plugins.set('HtmlWebpackPlugin', new HtmlWebpackPlugin({
  title: 'My ReactJs App',
  inject: true
}))
module.exports = environment.toWebpackConfig()
